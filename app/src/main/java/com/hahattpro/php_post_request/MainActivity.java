package com.hahattpro.php_post_request;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class MainActivity extends Activity {
    String POST_LOG_TAG = "POST REQUEST";
    String DOWNLOAD_LOG_TAG ="DOWNLOAD";

    String uuid;
    HttpURLConnection connection;
    URL url;


    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        uuid = tManager.getDeviceId();
        textView = (TextView) findViewById(R.id.status);

        new POST_REQUEST().execute();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    InputStream in - InputStream of json file from server
    Return file_link - string of file object in JSON
     */
    private String ReadInputStream(InputStream in) {
        String file_link = null;
        String tmp = null;
        String out = null;
        try {

            InputStreamReader inputStreamReader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder builder = new StringBuilder();
            while ((tmp = bufferedReader.readLine()) != null) {
                builder.append(tmp);
            }
            out = builder.toString();
            Log.i(POST_LOG_TAG, out);

            String rawJson = out;
            JSONObject reader = new JSONObject(rawJson);
            file_link = reader.getString("file");
            file_link = "http://"+file_link;
            Log.i(POST_LOG_TAG, file_link);
        } catch (IOException e) {
            e.getStackTrace();
        } catch (JSONException e) {
            e.getStackTrace();
        }
        return file_link;
    }

    private class POST_REQUEST extends AsyncTask<Void, Void, Void> {
        String file_link = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i(POST_LOG_TAG,"Start Post Request");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                url = new URL("http://128.199.127.222:1212/index.php");//server URL

                //start connection
                connection = (HttpURLConnection) url.openConnection();

                //timed out when it wait too long
                connection.setReadTimeout(100000);
                connection.setConnectTimeout(150000);

                //POST method
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);

                /*
                * url: String - Website link
                * uuid: String - Device ID
                * note: String - Comment for the website link
                * */
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("url", "http://kairopark.jp/android/en/")
                        .appendQueryParameter("uuid", uuid)
                        .appendQueryParameter("note", "Keep calm and meow on");
                String query = builder.build().getEncodedQuery();

                //Put Uri with request to connection outputStream
                //This is what we send to server
                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();

                //get input stream from server
                InputStream in = connection.getInputStream();
                file_link = ReadInputStream(in);//read input stream and return a link
                connection.disconnect();
            } catch (MalformedURLException e) {
            } catch (IOException e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            Log.i(POST_LOG_TAG, "Done Post Request");
            new DownloadFileFromURL().execute(file_link);

        }
    }


    private class DownloadFileFromURL extends AsyncTask<String, Void, Void> {
        URL url;
        String save_dir;
        private String DOWNLOAD_LOG_TAG = "DOWNLOAD";
        String FILE_NAME ="WEB2PDF_SAVED_";
        String FILE_EXTENSION = ".pdf";
        int num = 0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i(DOWNLOAD_LOG_TAG, "Start Download");
        }

        @Override
        protected Void doInBackground(String... strings) {
            int count;
            try {
                URL url = new URL( strings[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                save_dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
                Log.i(DOWNLOAD_LOG_TAG, "save_dir = " + save_dir);

                //Check dublicate
                File file = new File(save_dir+"/"+FILE_NAME+num+FILE_EXTENSION);
                while (file.exists())
                {
                    num++;
                    file = new File(save_dir+"/"+FILE_NAME+Integer.toString(num)+FILE_EXTENSION);
                }
                OutputStream output = new FileOutputStream(file);//open file


                //Write to file
                byte data[] = new byte[1024];
                while ((count = input.read(data)) != -1) {
                    // writing data to file
                    output.write(data, 0, count);
                }

                Log.i(DOWNLOAD_LOG_TAG,"File path = "+file.getPath());

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i(DOWNLOAD_LOG_TAG, "Done Download");
            textView.setText("ALL done");
        }
    }


}
